let userLoged = null

fetch('http://localhost/users?pseudo=MALLORY')
  .then(response => response.json())
  .then(data => {
    userLoged = data.pseudo
})

document.getElementById("addcard").addEventListener('click',(event)=>{
  const data = {
    "name": document.getElementById("name").value,
    "pseudo_owner": userLoged,
    "img_url": document.getElementById("img_url").value,
    "description": document.getElementById("description").value,
    "hp": document.getElementById("hp").value,
    "energy": document.getElementById("energy").value,
    "defense": document.getElementById("defense").value,
    "attack": document.getElementById("attack").value,
    "price": document.getElementById("price").value,
    "family_name": document.getElementById("family_name").value,
  };
  fetch('http://localhost/cards', {
    method: 'POST',
    headers: {'Content-Type': 'application/json',},
    body: JSON.stringify(data),
  })
  .then(response => response.json())
  .then(data => {
      alert("Vous avez créer la carte n°" + data.id_card);
  })
  .catch((error) => {
      console.error('Error:', error);
  });
});