package org.cardsmarket.sp.card.data;

import org.cardsmarket.sp.card.dao.CardDAO;
import org.cardsmarket.sp.card.model.Card;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataGenerator implements ApplicationRunner {

    private final CardDAO cardDAO;

    public DataGenerator(CardDAO cardDAO) {
        this.cardDAO = cardDAO;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (this.cardDAO.findAll().size() != 0) {
            System.out.println("Pas de traitement, déja des données en bdd");
            return;
        }

        Card card = new Card();
        card.setPseudoOwner("MALLORY");
        card.setName("Chat domestique");
        card.setImgUrl("https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid%3DOIP.YDMTEBn7QTiSF8HnOAe5RQHaEo%26pid%3DApi&f=1");
        card.setAttack(10);
        card.setDefense(5);
        card.setFamily_name("Félin");
        card.setDescription("Ceci est un chat domestique");
        card.setHp(100);
        card.setEnergy(300);
        card.setPrice(100);
        card.setAffinity("Lumière");
        this.cardDAO.save(card);

        Card card2 = new Card();
        card2.setPseudoOwner("MALLORY");
        card2.setName("Chien domestique");
        card2.setImgUrl("https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fstatic.cotemaison.fr%2Fmedias_10824%2Fw_1815%2Ch_1362%2Cc_crop%2Cx_99%2Cy_0%2Fw_1080%2Ch_600%2Cc_fill%2Cg_north%2Fv1456392403%2F10-conseils-pour-rendre-votre-chien-heureux_5542245.jpg&f=1&nofb=1");
        card2.setAttack(15);
        card2.setDefense(0);
        card2.setFamily_name("Canin");
        card2.setDescription("Ceci est un chien domestique");
        card2.setHp(200);
        card2.setEnergy(200);
        card2.setPrice(150);
        card2.setAffinity("Electrique");
        this.cardDAO.save(card2);
    }
}
