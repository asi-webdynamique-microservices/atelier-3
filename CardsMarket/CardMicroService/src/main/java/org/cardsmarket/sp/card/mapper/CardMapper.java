package org.cardsmarket.sp.card.mapper;

import org.cardsmarket.sp.card.model.Card;
import org.modelDTO.dto.CardDTO;
import org.springframework.stereotype.Component;
@Component
public class CardMapper {
        public CardDTO toDTO(Card card) {
            return new CardDTO(card.getId_card(), card.getName(), card.getPrice(), card.getFamily_name(), card.getDescription(), card.getAttack(),
                    card.getDefense(), card.getEnergy(), card.getHp(), card.getPseudoOwner(), card.getImgUrl(), card.getAffinity());
        }

        public Card toModel(CardDTO card) {
            return new Card(card.getId_card(), card.getName(), card.getPrice(), card.getFamily_name(), card.getDescription(), card.getAttack(),
                    card.getDefense(), card.getEnergy(), card.getHp(), card.getImg_url(), card.getAffinity());
        }
    }



