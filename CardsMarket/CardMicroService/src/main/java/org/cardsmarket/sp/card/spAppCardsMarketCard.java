package org.cardsmarket.sp.card;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class spAppCardsMarketCard {
    public static void main(String[] args) {

        SpringApplication.run(spAppCardsMarketCard.class,args);
    }
}