package org.cardsmarket.sp.card.dao;

import org.cardsmarket.sp.card.model.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;


public interface CardDAO extends JpaRepository<Card, Integer> {
        List<Card> findByPseudoOwner(String pseudo_owner);
    }

