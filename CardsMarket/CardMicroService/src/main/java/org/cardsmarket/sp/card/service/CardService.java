package org.cardsmarket.sp.card.service;

import org.modelDTO.dto.CardDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.cardsmarket.sp.card.model.Card;
import org.cardsmarket.sp.card.dao.CardDAO;

import java.util.List;
import java.util.Optional;
@Service
public class CardService {

        @Autowired
        CardDAO cardDAO;

        @Autowired
        ColorRestServiceClient colorService;

        public Card getCardbyId(Integer id) {
            Optional<Card> card = cardDAO.findById(id);//findById_Card
            return card.get();
        }

        public Card addCard(Card cardToAdd, String pseudo) {
            cardToAdd.setPseudoOwner(pseudo);
            Card card = cardDAO.save(cardToAdd);
            CardDTO cardDTO = colorService.getAffinity(card.getId_card(), card.getImgUrl());
            card.setAffinity(cardDTO.getAffinity());
            cardDAO.save(card);
            return card;
        }

        public List<Card> getCardsList() {
            return cardDAO.findAll();
        }

        public List<Card> getCardsByPseudoOwner(String pseudo) {
            return cardDAO.findByPseudoOwner(pseudo);
        }

        public Card updateCard(Card card) {
            return cardDAO.save(card);
        }
}


