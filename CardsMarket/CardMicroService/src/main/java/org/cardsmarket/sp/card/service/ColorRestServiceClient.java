package org.cardsmarket.sp.card.service;

import org.modelDTO.dto.CardDTO;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ColorRestServiceClient {
    public CardDTO getAffinity(Integer id_card, String url){
        final String URL = "http://localhost/colors";

        RestTemplate restTemplate = new RestTemplate();
        CardDTO card = new CardDTO();
        card.setId_card(id_card);
        card.setImg_url(url);
        return restTemplate.postForEntity(URL, card, CardDTO.class).getBody();
    }
}
