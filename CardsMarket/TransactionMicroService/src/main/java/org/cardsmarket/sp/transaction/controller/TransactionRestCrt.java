package org.cardsmarket.sp.transaction.controller;

import org.cardsmarket.sp.transaction.mapper.TransactionMapper;
import org.cardsmarket.sp.transaction.model.Transaction;
import org.cardsmarket.sp.transaction.service.TransactionService;
import org.modelDTO.dto.TransactionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
public class TransactionRestCrt {


        @Autowired
        private TransactionService transactionService;
        @Autowired
        private TransactionMapper transactionMapper;

        //Sell a card
        @RequestMapping(method= RequestMethod.POST,value="/transactions")
        public TransactionDTO sellCard(@RequestBody TransactionDTO transaction){
            return transactionMapper.toDTO(transactionService.sellCard(transaction.getPseudo_seller(),
                    transaction.getId_card(), transaction.getDescription()));
        }

        //Buy a card
        @RequestMapping(method= RequestMethod.PUT,value="/transactions/{idTransaction}/buyer/{pseudoUser}")
        public TransactionDTO buyCard(@PathVariable int idTransaction, @PathVariable String pseudoUser){
            return transactionMapper.toDTO(transactionService.setBuyer(idTransaction, pseudoUser));
        }

        //Get all transactions
        @RequestMapping(method=RequestMethod.GET,value="/transactions")
        public List<TransactionDTO> getAllTransactions(){
            List<TransactionDTO> allTransactionDTO = new ArrayList<TransactionDTO>();
            for (Transaction t : transactionService.getAllTransactions()){
                allTransactionDTO.add(transactionMapper.toDTO(t));
            }
            return allTransactionDTO;
        }
    }

