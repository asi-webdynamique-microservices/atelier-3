package org.cardsmarket.sp.transaction.service;


import org.modelDTO.dto.CardDTO;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CardRestServiceClient {

    public CardDTO getCardbyId(Integer id){

        final String URL_USERS = "http://localhost/cards?id={id}";

        RestTemplate restTemplate = new RestTemplate();

        // Send request with GET method and default Headers.
        return restTemplate.getForObject(URL_USERS, CardDTO.class, id);

    }

    public void updateCard(Integer card){

        final String URL_USERS = "http://localhost/cards";

        RestTemplate restTemplate = new RestTemplate();

        // Send request with GET method and default Headers.
        restTemplate.put(URL_USERS, CardDTO.class, card);


    }

    public void changeCardOwner(Integer id_card, String pseudoOwner) {
        final String URL_USERS = "http://localhost/cards";

        RestTemplate restTemplate = new RestTemplate();
        CardDTO card = new CardDTO();
        card.setId_card(id_card);
        card.setPseudo_owner(pseudoOwner);

        // Send request with GET method and default Headers.
        restTemplate.put(URL_USERS, card, CardDTO.class);
    }
}