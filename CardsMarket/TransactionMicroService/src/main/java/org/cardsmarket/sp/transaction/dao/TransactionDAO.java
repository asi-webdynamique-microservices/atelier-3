package org.cardsmarket.sp.transaction.dao;

import org.cardsmarket.sp.transaction.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionDAO extends JpaRepository<Transaction, Integer> {

        List<Transaction> findByPseudoBuyer(String userBuyer);
        List<Transaction> findByPseudoSeller(String userSeller);
        List<Transaction> findByCardId(Integer card);
    }


