package org.cardsmarket.sp.transaction.model;

import javax.persistence.*;

@Entity
public class Transaction {

        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        private int id_transaction;

        private String pseudoBuyer;
        private String pseudoSeller;

        private Integer cardId;
        private String description;

        public Transaction(String userBuyer, String userSeller, Integer card, String description) {
            this.pseudoBuyer = userBuyer;
            this.pseudoSeller = userSeller;
            this.cardId = card;
            this.description = description;
        }

        public Transaction() {
        }

        public int getId_transaction() {
            return id_transaction;
        }

        public void setId_transaction(int id_transaction) {
            this.id_transaction = id_transaction;
        }

        public String getPseudoBuyer() {
            return pseudoBuyer;
        }

        public void setPseudoBuyer(String pseudoBuyer) {
            this.pseudoBuyer = pseudoBuyer;
        }

        public String getPseudoSeller() {
            return pseudoSeller;
        }

        public void setPseudoSeller(String pseudoSeller) {
            this.pseudoSeller = pseudoSeller;
        }

        public Integer getCardId() {
            return cardId;
        }

        public void setCardId(Integer cardId) {
            this.cardId = cardId;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

