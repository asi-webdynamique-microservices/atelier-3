package org.cardsmarket.sp.transaction.mapper;

import org.cardsmarket.sp.transaction.model.Transaction;
import org.cardsmarket.sp.transaction.service.CardRestServiceClient;
import org.cardsmarket.sp.transaction.service.UserRestServiceClient;
import org.modelDTO.dto.TransactionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class TransactionMapper {

        @Autowired
        UserRestServiceClient userService;

        public TransactionDTO toDTO(Transaction transaction){
            TransactionDTO transactionDTO =  new TransactionDTO(transaction.getId_transaction(), transaction.getPseudoSeller(), transaction.getCardId(), transaction.getDescription());
            if(transaction.getPseudoBuyer() != null){
                transactionDTO.setPseudo_buyer(transaction.getPseudoBuyer());
            }
            return transactionDTO;
        }
        public Transaction toModel(TransactionDTO transactionDTO){
            return new Transaction(userService.findUser(transactionDTO.getPseudo_buyer()),
                    userService.findUser(transactionDTO.getPseudo_seller()), transactionDTO.getId_card(),
                    transactionDTO.getDescription());
        }
    }

