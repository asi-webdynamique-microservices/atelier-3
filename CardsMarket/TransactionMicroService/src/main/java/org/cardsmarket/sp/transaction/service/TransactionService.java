package org.cardsmarket.sp.transaction.service;

import org.cardsmarket.sp.transaction.dao.TransactionDAO;
import org.cardsmarket.sp.transaction.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TransactionService {


        @Autowired
        TransactionDAO transactionDao;

        @Autowired
        UserRestServiceClient userService;
        @Autowired
        CardRestServiceClient cardService;

        public Transaction sellCard(String pseudoSeller, Integer id_card, String description){
            Transaction transaction =  new Transaction(null, userService.findUser(pseudoSeller), id_card, description);
            transaction.setPseudoSeller(pseudoSeller);
            cardService.changeCardOwner(id_card,"CardsMarket");
            return transactionDao.save(transaction);
        }

        public List<Transaction> getAllTransactions() {
            return transactionDao.findAll();
        }

        public List<Transaction> getAllTransactionWithoutBuyer (){
            return transactionDao.findByPseudoBuyer(null);
        }

        public Transaction getTransaction (int id) {
            Optional<Transaction> transactionOptional = transactionDao.findById(id);
            if (transactionOptional.isPresent())
                return transactionOptional.get();
            else
                return null;
        }

        public List<Transaction> getTransactionByUserBuyer (String user){
            return transactionDao.findByPseudoBuyer(user);
        }


        public List<Transaction> getTransactionByUserSeller (String user){
            return transactionDao.findByPseudoSeller(user);
        }

        public List<Transaction> getTransactionByCard (Integer card){
            return transactionDao.findByCardId(card);
        }

        public Transaction updateTransaction (Transaction transactionToUpdate){
            return transactionDao.save(transactionToUpdate);
        }

        public Transaction setBuyer (int idTransaction, String pseudoBuyer){
            Transaction transaction = getTransaction(idTransaction);
            transaction.setPseudoBuyer(userService.findUser(pseudoBuyer));
            return transaction;
        }
    }


