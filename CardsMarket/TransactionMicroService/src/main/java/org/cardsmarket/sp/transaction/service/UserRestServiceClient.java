package org.cardsmarket.sp.transaction.service;

import org.modelDTO.dto.UserDTO;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserRestServiceClient {

    public String findUser(String pseudo){

        final String URL_USERS = "http://localhost/users?pseudo={pseudo}";

        RestTemplate restTemplate = new RestTemplate();

        // Send request with GET method and default Headers.

        return restTemplate.getForObject(URL_USERS, String.class, pseudo);


    }


}