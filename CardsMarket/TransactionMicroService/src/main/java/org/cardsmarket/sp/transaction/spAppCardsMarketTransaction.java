package org.cardsmarket.sp.transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class spAppCardsMarketTransaction {
    public static void main(String[] args) {

        SpringApplication.run(spAppCardsMarketTransaction.class,args);
    }
}