package org.cardsmarket.sp.user.data;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.cardsmarket.sp.user.dao.UserDAO;
import org.cardsmarket.sp.user.model.User;


@Component
public class DataGenerator implements ApplicationRunner {

    private final UserDAO userDAO;

    public DataGenerator(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (this.userDAO.findAll().size() != 0) {
            System.out.println("Pas de traitement, déja des données en bdd");
            return;
        }

        User user2 = new User();
        user2.setPseudo("MALLORY");
        user2.setWallet(1000);
        this.userDAO.save(user2);

        User user3 = new User();
        user3.setPseudo("CardsMarket");
        user3.setWallet(1000000);
        this.userDAO.save(user3);


    }
}

