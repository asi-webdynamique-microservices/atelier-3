package org.cardsmarket.sp.user.service;

import org.cardsmarket.sp.user.dao.UserDAO;
import org.cardsmarket.sp.user.mapper.UserMapper;
import org.cardsmarket.sp.user.model.User;
import org.modelDTO.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserDAO userDAO;

    @Autowired
    UserMapper userMapper;

    public UserDTO addUser(User userToAdd){
        return userMapper.toDTO(userDAO.save(userToAdd));
    }

    public UserDTO findUser(String pseudo){
        return userMapper.toDTO(userDAO.findById(pseudo)
                .orElseThrow(() -> new RuntimeException("Pas de user pour le login " + pseudo)));
    }

    public void deleteUser(String pseudo){
        try{
            userDAO.deleteById(pseudo);
        }catch (Exception e){
            throw new RuntimeException("Impossible de supprimer le user pour le login " + pseudo);
        }
    }
}
