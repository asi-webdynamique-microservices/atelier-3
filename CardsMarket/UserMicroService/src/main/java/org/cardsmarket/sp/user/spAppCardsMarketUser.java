package org.cardsmarket.sp.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class spAppCardsMarketUser {
    public static void main(String[] args) {

        SpringApplication.run(spAppCardsMarketUser.class,args);
    }
}