package org.cardsmarket.sp.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class User {

        @Id
        @Column(length = 100)
        private String pseudo;
        @Column(length = 100)
        private String name;
        @Column(length = 100)
        private String surname;
        @Column(length = 100)
        private String password;
        @Column(length = 100)
        private String mail;
        private Integer wallet;

        public User() {
        }

        public User(String pseudo, String name, String surname, String mail, Integer wallet) {
            this.pseudo = pseudo;
            this.name = name;
            this.surname = surname;
            this.mail = mail;
            this.wallet = wallet;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getMail() {
            return mail;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }

        public Integer getWallet() {
            return wallet;
        }

        public void setWallet(Integer wallet) {
            this.wallet = wallet;
        }

        public String getPseudo() {
            return pseudo;
        }

        public void setPseudo(String pseudo) {
            this.pseudo = pseudo;
        }
    }

