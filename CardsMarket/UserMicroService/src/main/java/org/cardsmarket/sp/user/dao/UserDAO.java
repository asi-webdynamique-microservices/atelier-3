package org.cardsmarket.sp.user.dao;
import org.cardsmarket.sp.user.model.User;
import org.modelDTO.dto.UserDTO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface UserDAO extends JpaRepository<User, String> {

}
