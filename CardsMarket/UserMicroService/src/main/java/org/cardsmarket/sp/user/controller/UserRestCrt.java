package org.cardsmarket.sp.user.controller;

import org.cardsmarket.sp.user.mapper.UserMapper;
import org.cardsmarket.sp.user.model.User;
import org.cardsmarket.sp.user.service.UserService;
import org.modelDTO.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserRestCrt {


        @Autowired
        UserService userService;

        @Autowired
        UserMapper userMapper;

        // Create User
        @RequestMapping(method = RequestMethod.POST, value = "/users")
        public UserDTO addUser(@RequestBody User user){
            return userService.addUser(user);
        }

        @GetMapping("/users")
        public UserDTO getUserByPseudo(@RequestParam(name = "pseudo") String pseudo){
            return userService.findUser(pseudo);
        }

        @DeleteMapping("/users")
        public void deleteUserByPseudo(@RequestParam(name = "pseudo") String pseudo){
            userService.deleteUser(pseudo);
        }
    }



