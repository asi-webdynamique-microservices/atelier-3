package org.cardsmarket.sp.user.mapper;

import org.cardsmarket.sp.user.model.User;
import org.modelDTO.dto.UserDTO;
import org.springframework.stereotype.Component;
@Component
public class UserMapper {
        public UserDTO toDTO(User user){
            return new UserDTO(user.getPseudo(), user.getName(), user.getSurname(), user.getMail(), user.getWallet());
        }
        public User toModel(UserDTO userDTO){
            return new User(userDTO.getPseudo(), userDTO.getName(), userDTO.getSurname(), userDTO.getMail(), userDTO.getWallet());
        }
    }



