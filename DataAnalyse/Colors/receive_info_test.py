#!/usr/bin/env python
# import main Flask class and request object
from flask import Flask, request
import urllib.request
from PIL import Image

##An additional argument for an optional image mask image is also available.
# create the Flask app
app = Flask(__name__)

from PIL import Image
from PIL.ExifTags import TAGS
from os import listdir
#from mlxtend.frequent_patterns import apriori, association_rules
from webcolors import rgb_to_name,CSS3_HEX_TO_NAMES,hex_to_rgb, CSS3_NAMES_TO_HEX,HTML4_HEX_TO_NAMES,_reversedict #!pip install webcolors

@app.route('/cards', methods=['GET','POST'])
def recup():
    request_data = request.get_json()
    affinite = None
    url_card=None
    objet_detect=None
    if request_data:
        if 'url_card' in request_data:
            url_card = request_data['url_card']
            print(url_card)
        if 'affinite' in request_data:
            affinite = request_data['affinite']
            print(affinite)
        if 'objet_detect' in request_data:
            objet_detect = request_data['objet_detect']
            print(affinite)
    return '''The objet_detect value is: {}
           The url of the card is: {}
           Color prim algo : {}
           '''.format(url_card,affinite,objet_detect)

@app.route('/help', methods=['GET','POST'])
def help():
    return 'ysvdvdusbfbd'

if __name__ == '__main__':
    # run app in debug mode on port 5000
    app.run(debug=True, port=8082)
    
