#!/usr/bin/env python
# import main Flask class and request object
from flask import Response
import json
import os
import requests
from PIL.ExifTags import TAGS
import numpy as np
from sklearn.cluster import MiniBatchKMeans
from flask import Flask, request
import urllib.request
from PIL import Image

# An additional argument for an optional image mask image is also available.
# create the Flask app
app = Flask(__name__)
#import traceback
#import sys
#from mlxtend.frequent_patterns import apriori, association_rules
# from webcolors import hex_to_rgb, CSS3_NAMES_TO_HEX,_reversedict #!pip install webcolors
#from scipy.spatial import KDTree
#import wget


# def test_json(affinite):
#                     json_url = "{\"id_card\":\"" + str(id_card)+"\",\"affinite\":\""+str(affinite)+"\"}"
#     print(json_url)
#     response = requests.post("http://localhost:8082/cards", params=json_url)
#     print(response.status_code)


AFFINITE_NAMES_TO_HEX = {
    "black": "#000000",
    "blue": "#0000ff",
    "brown": "#a52a2a",
    "gray": "#808080",
    "green": "#008000",
    "orange": "#ffa500",
    "pink": "#ffc0cb",
    "purple": "#800080",
    "red": "#ff0000",
    "violet": "#ee82ee",
    "white": "#ffffff",
    "yellow": "#ffff00",
}

'''
COLOR_TO_AFFINITE={"black": "tenebre",
    "blue": "eau",
    "cyan": "glace",
    "green": "plante",
    "pink": "psy",
    "red": "feu",
    "white": "lumiere",
    "yellow": "electrique",}
'''


def proche_couleur(liste_rgb):
    val_affinite = None
    if liste_rgb[0] <= 128 and liste_rgb[0] >= 0:
        if liste_rgb[1] <= 128 and liste_rgb[1] >= 0:
            if liste_rgb[2] <= 128 and liste_rgb[2] >= 0:
                print("Tenebre")
                val_affinite = "Tenebre"
            elif liste_rgb[2] <= 255 and liste_rgb[2] >= 129:
                print("Eau")
                val_affinite = "Eau"
        elif liste_rgb[1] <= 255 and liste_rgb[1] >= 129:
            if liste_rgb[2] <= 128 and liste_rgb[2] >= 0:
                print("Plante")
                val_affinite = "Plante"
            elif liste_rgb[2] <= 255 and liste_rgb[2] >= 129:
                print("Glace")
                val_affinite = "Glace"
    elif liste_rgb[0] <= 255 and liste_rgb[0] >= 129:
        if liste_rgb[1] <= 128 and liste_rgb[1] >= 0:
            if liste_rgb[2] <= 128 and liste_rgb[2] >= 0:
                print("Feu")
                val_affinite = "Feu"
            elif liste_rgb[2] <= 255 and liste_rgb[2] >= 129:
                print("Psy")
                val_affinite = "Psy"
        elif liste_rgb[1] <= 255 and liste_rgb[1] >= 129:
            if liste_rgb[2] <= 128 and liste_rgb[2] >= 0:
                print("Electrique")
                val_affinite = "Electrique"
            elif liste_rgb[2] <= 255 and liste_rgb[2] >= 129:
                print("Lumiere")
                val_affinite = "Lumiere"
    return val_affinite


'''
def convert_rgb_to_names(rgb_tuple):

    AFFINITE_HEX_TO_NAMES = _reversedict(AFFINITE_NAMES_TO_HEX)
    db_name_color = AFFINITE_HEX_TO_NAMES
    names_color = []
    rgb_values = []
    for color_hex, color_name in db_name_color.items():
        names_color.append(color_name)
        rgb_values.append(hex_to_rgb(color_hex))
    # Using Decision tree algorithme to determine nearest color name
    kdt_db = KDTree(rgb_values)
    distance, index_color = kdt_db.query(rgb_tuple)
    return names_color[index_color]
'''


def KMeanscolors_annotations(clusters_number, image):
    verbose = False
    # On resize l'image :
    Xsize = image.size[0]
    Ysize = image.size[1]
    Xresize = Xsize % 17
    Yresize = Ysize % 17
    Xresize = int(Xsize * 17/100)
    Yresize = int(Ysize * 17/100)
    resized_tuple = (Xresize, Yresize)
    resized_image = image.resize(resized_tuple)

    if verbose:
        print(resized_tuple)
    if verbose:
        print(resized_image.size)

    pixels = np.array(list(resized_image.getdata()))
    kmeans = MiniBatchKMeans(n_clusters=clusters_number)
    kmeans = kmeans.fit(pixels)
    centroids = kmeans.cluster_centers_
    if verbose:
        print(centroids[0], centroids[1])
    primary_color = centroids[0].tolist()
    secondary_color = centroids[1].tolist()
    # print(primary_color)
    # print(secondary_color)
    #print(primary_color, secondary_color)
    #centroids_list = centroids.tolist()
    # print(centroids_list)
    return primary_color, secondary_color


@app.route('/help', methods=['GET'])
def help():
    return 'help me'

# GET requests will be blocked


@app.route('/colors', methods=['POST'])
def json_example():
    request_data = request.get_json()
    json_url = "{\"id_card\":\"error\",\"affinity\":\"error\"}"
    language = None
    img_url = None
    color_primary = None
    color_secondary = None
    prim_color = None
    sec_color = None
    color_prim_ = None
    color_sec_ = None
    # color_primary=None
    if request_data:
        '''if 'language' in request_data:
            language = request_data['language']
            test_json(language)
        '''
        if 'img_url' in request_data:
            img_url = request_data['img_url']
            try:
                lien_image = "Image.jpg"
                try:
                    os.makedirs('tmp_img', exist_ok=True)
                except FileExistsError:
                    pass
                '''
                test_api.callAPI()
                name_image_local=str(img_url)
                image = Image.open("images/"+name_image_local)
                '''  # lorsque l'url ne marche pas car protection du site contre les bots, on utilise des files que l'on telecharge
                urllib.request.urlretrieve(img_url, "tmp_img/"+lien_image)
                '''file_name=wget.download(img_url)
                print(file_name)'''
                print("download successful")
                image = Image.open("tmp_img/"+lien_image)
                print("open image successful")
                prim_color, sec_color = KMeanscolors_annotations(2, image)
                # color_primary=convert_rgb_to_names(prim_color) #ancienne methode
                # color_secondary=convert_rgb_to_names(sec_color) #ancienne methode
                color_prim_ = proche_couleur(prim_color)
                color_sec_ = proche_couleur(sec_color)
                affinite = color_prim_
                # test_json(affinite)
                id_card = request_data['id_card']
                json_url = "{\"id_card\":\"" + \
                    str(id_card)+"\",\"affinity\":\""+str(affinite)+"\"}"
            except IOError:
                print("error")
                # print(traceback.format_exc())
                pass
    return Response(json_url, mimetype='application/json')


if __name__ == '__main__':
    # run app in debug mode on port 5000
    app.run(debug=True, port=8085)
