import os
import requests
import json
import io

####################
######Fonctions######
####################

## Executer la requête API pour récupérer les informations des photos d'animaux sur le site de pexels.com
def initqueryAPI(query,perPage,numPage):
  url = "https://api.pexels.com/v1/search"
  querystring = {"query":query,"per_page":perPage,"page":numPage}
  payload = ""
  headers = {"Authorization": "563492ad6f91700001000001995fd381cacb4410889673c2402ba4ec"} #563492ad6f9170000100000148c73910ab8e4539a980250c704e9967 
  response = requests.request("GET", url, data=payload, headers=headers, params=querystring)
  return response

def callAPI():
  querys=['boat','cat','dog','rabbit','frog','duck']
  picturesTagData = {}
  counter=0
  try:
    os.makedirs('./images')
    Verbose=True
  except FileExistsError:
    Verbose=False
    pass
  
  if Verbose == True:
    for query in querys:
      nbPhotoWanted=20
      numPage=1
      #Tant qu'on à pas récupérer le nombre d'image voulu, passer à la page suivante
      while nbPhotoWanted > 0:
        response = initqueryAPI(query,nbPhotoWanted,numPage)
        dataPictures = json.loads(response.text)
        #On parcours les données pour récupéré chaque photo
        for pictureData in dataPictures["photos"]:
          #On recupère l'url et le nom de la photo
          urlPicture = pictureData["src"]["medium"]
          pictureName = pictureData["src"]["medium"].split("/")[-1]
          #On recupère la photo original en binaire
          picture = requests.get(urlPicture).content
          #On écrit la photo en binaire dans un fichier
          counter+=1
          pictureName=str(counter)
          with open("./images/"+pictureName, 'wb') as handler:
            handler.write(picture)
          handler.close()
          picturesTagData[pictureName] = {'tag' : [query]}
          #On met à jour notre nombre de photo voulu
          nbPhotoWanted -= 1
        #On passe à la page suivante
        numPage += 1
        
    file = open('imagesData.json', 'w')
    json.dump(picturesTagData, file, indent = 6)
    file.close()